package br.com.mentorama.LojaRoupa.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ProductTest {

    @Test
    public void precoTotalComDesconto(){
        Product product = new Product(1, "Blusa", 100.0, 0.20, 5);
        Double resultado = product.getPrecoComDesconto(0.20);
        assertEquals(80.0,resultado);
    }

    @Test
    public void descontoMaiorQueDescontoMax(){
        Product product = new Product(1, "Blusa", 100.0, 0.20, 5);
        Double resultado = product.getPrecoComDesconto(0.25);
        assertEquals(80.0, resultado);
    }

    @Test
    public void descontoMenorQueDescontoMax(){
        Product product = new Product(1, "Blusa", 100.0, 0.20, 5);
        Double resultado = product.getPrecoComDesconto(0.10);
        assertEquals(90.0, resultado);
    }
}
