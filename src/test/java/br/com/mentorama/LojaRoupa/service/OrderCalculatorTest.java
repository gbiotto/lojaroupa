package br.com.mentorama.LojaRoupa.service;

import br.com.mentorama.LojaRoupa.model.Order;
import br.com.mentorama.LojaRoupa.model.OrderItem;
import org.junit.jupiter.api.*;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;
public class OrderCalculatorTest {

    private final OrderCalculator orderCalculator;

    public OrderCalculatorTest(){
        System.out.println("Construtor");
        this.orderCalculator = new OrderCalculator();
    }
    @BeforeEach
    void setUp() {
        System.out.println("Execução de teste Antes de cada um");
    }

    @BeforeAll
    static void beforeAll() {
        System.out.println("Before All");
    }

    @AfterAll
    static void afterAll() {
        System.out.println("After All");
    }

    @AfterEach
    void afterEach(){
        System.out.println("After Each");
    }

    @Test
    public void calcularPrecoTotal(){
        final Order order = new Order(aListOfOrderItems());
        Double resultado = orderCalculator.calculateOrder(order);
        assertEquals(30.0, resultado);
    }

    private List<OrderItem> aListOfOrderItems(){
        return Arrays.asList(
                aOrderItem(20.0),
                aOrderItem(10.0)
        );
    }

    private OrderItem aOrderItem(final Double valorEsperado){
        OrderItem orderItem = mock(OrderItem.class);
        when(orderItem.precoTotal()).thenReturn(valorEsperado);
        return orderItem;
    }
}
