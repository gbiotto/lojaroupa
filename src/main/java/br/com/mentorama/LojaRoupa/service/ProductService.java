package br.com.mentorama.LojaRoupa.service;

import br.com.mentorama.LojaRoupa.ProductClienteAPI;
import br.com.mentorama.LojaRoupa.entities.PedidoEntity;
import br.com.mentorama.LojaRoupa.entities.ProdutoEntity;
import br.com.mentorama.LojaRoupa.model.Product;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductService {

    @Autowired
    private ProductClienteAPI cliente;
    private List<ProdutoEntity> produtoEntities = new ArrayList<>();

    public List<Product> findAll(){
        return cliente.findAll();
    }

    public List<Product> findPrice(){
        return cliente.findPrice();
    }

    public List<Product> findById(){ return cliente.findById();}

    public List<ProdutoEntity> verificarId(ProdutoEntity produto){
        if (produto.getId_produto() != null && produtoEntities.contains(produto.getId_produto())){
                System.out.println("Produto ja cadastrado");
            }
        else{
            System.out.println("Erro ao localizar id do produto");
        }
        return this.produtoEntities;
    }
}
