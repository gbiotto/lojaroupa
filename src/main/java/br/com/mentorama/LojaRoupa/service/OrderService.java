package br.com.mentorama.LojaRoupa.service;

import br.com.mentorama.LojaRoupa.ProductClienteAPI;
import br.com.mentorama.LojaRoupa.entities.PedidoEntity;
import br.com.mentorama.LojaRoupa.model.Product;
import br.com.mentorama.LojaRoupa.repository.PedidoRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrderService {

    @Autowired
    private PedidoRepository pedidoRepository;

    public OrderService(PedidoRepository pedidoRepository) {
        this.pedidoRepository = pedidoRepository;
    }

    public List<PedidoEntity>findAll(){
        return this.pedidoRepository.findAll();
    }

    public void add(final PedidoEntity pedido){
        if(pedido.getId_pedido() != null){
            pedido.setId_pedido(pedido.getId_pedido());
        }
    }
}
