package br.com.mentorama.LojaRoupa.service;

import br.com.mentorama.LojaRoupa.model.Email;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

@Component
public class Gateway {

    @JmsListener(destination = "mailbox", containerFactory = "myFactory")
    public void receiveMessage(Email email){
        if(email.getTo() == null || email.getBody() == null || email.getOrderItems() == null){
            throw new RuntimeException("Invalid email");
        }
        System.out.println("Received email from queue<" + email + ">");
    }

    @JmsListener(destination = "topic.mailbox", containerFactory = "topicListennerFactory")
    public void receiveMessageFromTopic(Email email){
        System.out.println("Received email from topic<" + email + ">");
    }
}
