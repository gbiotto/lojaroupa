package br.com.mentorama.LojaRoupa.service;

import br.com.mentorama.LojaRoupa.entities.ProdutoEntity;
import br.com.mentorama.LojaRoupa.model.OrderItem;
import br.com.mentorama.LojaRoupa.model.Product;
import org.springframework.stereotype.Service;

@Service
public class EstoqueService {

    private ProdutoEntity produtoEntity;
    private ProductService productService;
    private OrderItem orderItem;

    public void quantidadeDisponivel(final Product product){
        if (product.getQuantidade_dis() >= orderItem.getQuantidade_pedido()){
            System.out.println("Quantidade disponivel para venda");
        }
        else{
            System.out.println("Quantidade indisponivel para venda");
        }
    }

}
