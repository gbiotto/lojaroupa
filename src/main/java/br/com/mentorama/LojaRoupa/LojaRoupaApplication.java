package br.com.mentorama.LojaRoupa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LojaRoupaApplication {

	public static void main(String[] args) {
		SpringApplication.run(LojaRoupaApplication.class, args);
	}

}
