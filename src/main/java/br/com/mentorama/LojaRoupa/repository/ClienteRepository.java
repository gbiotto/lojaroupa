package br.com.mentorama.LojaRoupa.repository;

import br.com.mentorama.LojaRoupa.entities.ClienteEntity;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface ClienteRepository extends JpaRepository<ClienteEntity, Long> {

    List<ClienteEntity> findAllCliente();

    @Query(value = "select * from cliente c where nome_cliente = ?1", nativeQuery = true)
    List<ClienteEntity> findByNomeContaining(String nome_cliente);
}
