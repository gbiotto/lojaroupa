package br.com.mentorama.LojaRoupa.repository;

import br.com.mentorama.LojaRoupa.entities.ItensPedidoEntity;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface ItensPedidoRepository extends JpaRepository<ItensPedidoEntity, Long> {

    @Query(value = "select * from ItensPedidoEntity ip left join p.produto on ip.id_produto = p.id_produto")
    List<ItensPedidoEntity> findItensPedido();


}
