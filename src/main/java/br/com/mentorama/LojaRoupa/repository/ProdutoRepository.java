package br.com.mentorama.LojaRoupa.repository;

import br.com.mentorama.LojaRoupa.entities.ProdutoEntity;
import br.com.mentorama.LojaRoupa.vos.ProdutoVO;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProdutoRepository extends JpaRepository<ProdutoEntity, Long> {
    List<ProdutoEntity> findAllProduto(String produto);

    List<ProdutoVO> findById();
}
