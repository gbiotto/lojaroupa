package br.com.mentorama.LojaRoupa.vos;

public class ClienteVO {

    private Integer id_cliente;
    private String nome_cliente;
    private Integer telefone;

    public Integer getId_cliente() {
        return id_cliente;
    }

    public void setId_cliente(Integer id_cliente) {
        this.id_cliente = id_cliente;
    }

    public String getNome_cliente() {
        return nome_cliente;
    }

    public void setNome_cliente(String nome_cliente) {
        this.nome_cliente = nome_cliente;
    }

    public Integer getTelefone() {
        return telefone;
    }

    public void setTelefone(Integer telefone) {
        this.telefone = telefone;
    }

    @Override
    public String toString() {
        return "ClienteVO{" +
                "id_cliente=" + id_cliente +
                ", nome_cliente='" + nome_cliente + '\'' +
                ", telefone=" + telefone +
                '}';
    }
}
