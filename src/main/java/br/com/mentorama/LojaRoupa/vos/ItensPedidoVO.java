package br.com.mentorama.LojaRoupa.vos;

public class ItensPedidoVO {

    private Integer id_pedido;
    private Integer id_produto;
    private Integer quantidade;

    public Integer getId_pedido() {
        return id_pedido;
    }

    public void setId_pedido(Integer id_pedido) {
        this.id_pedido = id_pedido;
    }

    public Integer getId_produto() {
        return id_produto;
    }

    public void setId_produto(Integer id_produto) {
        this.id_produto = id_produto;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    @Override
    public String toString() {
        return "ItensPedidoVO{" +
                "id_pedido=" + id_pedido +
                ", id_produto=" + id_produto +
                ", quantidade=" + quantidade +
                '}';
    }

    public void setId_pedido(PedidoVO toPedidoVO){
    }

    public void setId_produto(ProdutoVO toProdutoVO){

    }
}
