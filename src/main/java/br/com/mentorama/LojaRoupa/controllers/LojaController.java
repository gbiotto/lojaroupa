package br.com.mentorama.LojaRoupa.controllers;

import br.com.mentorama.LojaRoupa.entities.ClienteEntity;
import br.com.mentorama.LojaRoupa.entities.ItensPedidoEntity;
import br.com.mentorama.LojaRoupa.model.OrderItem;
import br.com.mentorama.LojaRoupa.model.Product;
import br.com.mentorama.LojaRoupa.repository.ClienteRepository;
import br.com.mentorama.LojaRoupa.repository.ItensPedidoRepository;
import br.com.mentorama.LojaRoupa.repository.PedidoRepository;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.security.RolesAllowed;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/loja")
public class LojaController {

    private final ClienteRepository clienteRepository;
    private final PedidoRepository pedidoRepository;
    private final ItensPedidoRepository itensPedidoRepository;
    private final OrderItem orderItem;

    private List<Product> products;

    public LojaController(ClienteRepository clienteRepository, PedidoRepository pedidoRepository, ItensPedidoRepository itensPedidoRepository, OrderItem orderItem) {
        this.clienteRepository = clienteRepository;
        this.pedidoRepository = pedidoRepository;
        this.itensPedidoRepository = itensPedidoRepository;
        this.orderItem = orderItem;
    }

    public void Product(){
        this.products = new ArrayList<>();
    }

    @RolesAllowed("user")
    @GetMapping("/{nome_cliente}")
    public ClienteEntity findByName(@PathVariable("nome_cliente") final Long nome_cliente){
        return this.clienteRepository.getById(nome_cliente);
    }

    @GetMapping("/{itens_pedidos}")
    public List<ItensPedidoEntity> findAllByIdPedido(@PathVariable("id_pedido") final Long id_pedido){
        return this.itensPedidoRepository.findItensPedido();
    }

    @PostMapping
    public void createCliente(@RequestBody final ClienteEntity cliente){
        cliente.setId_cliente(cliente.getId_cliente());
        cliente.setNome_cliente(cliente.getNome_cliente());
        cliente.setTelefone(cliente.getTelefone());
    }

    @PostMapping
    public ResponseEntity<Integer> add(@RequestBody Product product){
        if(product.getId() == null && orderItem.getQuantidade_pedido() == null){
            System.out.println("Falha ao incluir produto no pedido");
        }
        products.add(product);
        return new ResponseEntity<>(product.getId(), HttpStatus.CREATED);
    }

}
