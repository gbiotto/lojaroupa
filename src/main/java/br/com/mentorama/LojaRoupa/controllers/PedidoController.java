package br.com.mentorama.LojaRoupa.controllers;

import br.com.mentorama.LojaRoupa.entities.ClienteEntity;
import br.com.mentorama.LojaRoupa.entities.ItensPedidoEntity;
import br.com.mentorama.LojaRoupa.entities.PedidoEntity;
import br.com.mentorama.LojaRoupa.entities.ProdutoEntity;
import br.com.mentorama.LojaRoupa.repository.ItensPedidoRepository;
import br.com.mentorama.LojaRoupa.repository.PedidoRepository;
import br.com.mentorama.LojaRoupa.repository.ProdutoRepository;
import java.util.List;
import javax.annotation.security.RolesAllowed;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

public class PedidoController {

    private final ProdutoRepository produtoRepository;
    private final PedidoRepository pedidoRepository;
    private final ItensPedidoRepository itensPedidoRepository;

    public PedidoController(ProdutoRepository produtoRepository, PedidoRepository pedidoRepository, ItensPedidoRepository itensPedidoRepository) {
        this.produtoRepository = produtoRepository;
        this.pedidoRepository = pedidoRepository;
        this.itensPedidoRepository = itensPedidoRepository;
    }

    private List<ItensPedidoEntity> itensPedidoEntities;

    @RolesAllowed("user")
    @GetMapping("/{id_pedido}")
    public PedidoEntity findByIdPedido(@PathVariable("id_pedido") final Long id_pedido){
        return this.pedidoRepository.getById(id_pedido);
    }
}
