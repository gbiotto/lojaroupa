package br.com.mentorama.LojaRoupa.controllers;


import br.com.mentorama.LojaRoupa.entities.ItensPedidoEntity;
import br.com.mentorama.LojaRoupa.entities.ProdutoEntity;
import br.com.mentorama.LojaRoupa.model.Email;
import br.com.mentorama.LojaRoupa.repository.ItensPedidoRepository;
import br.com.mentorama.LojaRoupa.repository.PedidoRepository;
import br.com.mentorama.LojaRoupa.repository.ProdutoRepository;
import br.com.mentorama.LojaRoupa.vos.ItensPedidoVO;
import br.com.mentorama.LojaRoupa.vos.ProdutoVO;
import java.util.List;
import java.util.stream.Collectors;
import javax.annotation.security.RolesAllowed;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/estoque")
public class EstoqueController {

    private final ProdutoRepository produtoRepository;
    private final PedidoRepository pedidoRepository;
    private final ItensPedidoRepository itensPedidoRepository;

    public EstoqueController(ProdutoRepository produtoRepository, PedidoRepository pedidoRepository, ItensPedidoRepository itensPedidoRepository) {
        this.produtoRepository = produtoRepository;
        this.pedidoRepository = pedidoRepository;
        this.itensPedidoRepository = itensPedidoRepository;
    }

    @RolesAllowed("admin")
    @PostMapping
    private ProdutoVO toProdutoVO(ProdutoEntity produto){
        ProdutoVO produtoVO = new ProdutoVO();

        produtoVO.setId_produto(produto.getId_produto());
        produtoVO.setNome_produto(produtoVO.getNome_produto());
        produtoVO.setDescricao(produtoVO.getDescricao());
        produtoVO.setValor(produtoVO.getValor());
        produtoVO.setDesc_max(produtoVO.getDesc_max());
        produtoVO.setQtd_disponivel(produtoVO.getQtd_disponivel());

        return produtoVO;
    }

    private ItensPedidoVO toItensPedidoVO(ItensPedidoEntity itensPedido){
        ItensPedidoVO itensPedidoVO = new ItensPedidoVO();

        itensPedidoVO.setId_pedido(itensPedidoVO.getId_pedido());
        itensPedidoVO.setId_produto(itensPedidoVO.getId_pedido());
        itensPedidoVO.setQuantidade(itensPedidoVO.getQuantidade());

        return itensPedidoVO;
    }

    @GetMapping
    private List<ProdutoVO> findAll(@RequestParam("page") Integer page,
                                    @RequestParam("pageSize") Integer pageSize){
        return this.produtoRepository.findAll(PageRequest.of(page, pageSize,
                Sort.by("nome_pruduto")))
                .stream()
                .map(this::toProdutoVO)
                .collect(Collectors.toList());

    }

    @GetMapping
    private List<ProdutoVO> findById(){
        return this.produtoRepository.findById().stream().collect(Collectors.toList());
    }

    @RolesAllowed("admin")
    @PutMapping
    public void update(@RequestBody ProdutoEntity produtoEntity){
        this.produtoRepository.save(produtoEntity);
    }

}
