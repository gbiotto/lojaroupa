package br.com.mentorama.LojaRoupa.model;

import java.util.List;

public class Email {

    private List<OrderItem> orderItems;

    public Email(List<OrderItem> orderItems) {
        this.orderItems = orderItems;
    }

    public List<OrderItem> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(List<OrderItem> orderItems) {
        this.orderItems = orderItems;
    }

    private String to;
    private String body;

    public Email() {
    }

    public Email(List<OrderItem> orderItems, String to, String body) {
        this.orderItems = orderItems;
        this.to = to;
        this.body = body;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    @Override
    public String toString(){
        return String.format("Email{to=%s, body=%s, produtos=%s}", getTo(), getBody(), orderItems);
    }
}
