package br.com.mentorama.LojaRoupa;

import br.com.mentorama.LojaRoupa.model.Product;
import br.com.mentorama.LojaRoupa.model.ProductDTO;
import br.com.mentorama.LojaRoupa.vos.ProdutoVO;
import java.util.List;
import java.util.stream.Collectors;
import javax.annotation.security.RolesAllowed;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;

@Component
public class ProductClienteAPI {

    @Value("${vendaProduto.url}")
    private String url;
    private String id;

    @RolesAllowed("user")
    public List<Product> findAll(){
        ResponseEntity<ProductDTO> responseEntity = new RestTemplate().getForEntity(url, ProductDTO.class);

        return responseEntity.getBody().getProducts();
    }

    @RolesAllowed("user")
    public List<Product> findPrice(){
        ResponseEntity<ProductDTO> responseEntity =
                new RestTemplate().getForEntity(url, ProductDTO.class);
        return responseEntity.getBody().getProducts();
    }

    public List<Product> findById() {
        ResponseEntity<ProductDTO> responseEntity = new RestTemplate().getForEntity(id, ProductDTO.class);
        return responseEntity.getBody().getProducts();
    }
}
